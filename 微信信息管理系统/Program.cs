using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using 微信信息管理系统.Code;
using 微信信息管理系统.Data;

var builder = WebApplication.CreateBuilder(args).Inject();

// Add services to the container.
builder.Services.AddCors(options => options.AddPolicy("CorsPolicy",
    builder =>
    {
        builder.AllowAnyMethod()
            .SetIsOriginAllowed(_ => true)
            .AllowAnyHeader()
            .AllowCredentials();
    }));
builder.Services.AddJwt();
builder.Services.AddControllers().AddInjectWithUnifyResult<AmisResultProvider>();
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();


IFreeSql fsql = new FreeSql.FreeSqlBuilder()
    .UseConnectionString(FreeSql.DataType.MySql, "Data Source=rm-bp164p33jz4uuf969125010om.mysql.rds.aliyuncs.com;Port=3306;User ID=yyywxnews;Password=Luohe0395;Initial Catalog=yyywxnews;Charset=utf8mb4;SslMode=none;Max pool size=10")
    .UseAutoSyncStructure(true) //自动同步实体结构到数据库
    .Build(); //请务必定义成 Singleton 单例模式
builder.Services.AddSingleton<IFreeSql>(fsql);


builder.Services.AddControllersWithViews()
    .AddNewtonsoftJson(options =>
    {
        //忽略空值
        // options.SerializerSettings.NullValueHandling= NullValueHandling.Ignore;
        //设置时间格式
        options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
        // 忽略属性的循环引用
        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        //不更改字符大小写
        options.SerializerSettings.ContractResolver = new DefaultContractResolver();
        //首字母小写
        // options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
    });


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseUnifyResultStatusCodes();

app.UseInject("api");

app.UseStaticFiles();

app.UseRouting();


app.UseCors("CorsPolicy");

app.UseAuthentication();
app.UseAuthorization();


app.MapBlazorHub();
app.MapControllers();
app.MapFallbackToPage("/_Host");

app.Run();
