﻿using FreeSql.DataAnnotations;

namespace 微信信息管理系统.Code.Model;

public class EntityBase
{
    [Column(IsIdentity = true, IsPrimary = true)]
    public int Id { get; set; }

    [Column(ServerTime = DateTimeKind.Local, CanUpdate = false)]
    public DateTime CreateTime { get; set; }

    [Column(ServerTime = DateTimeKind.Local)]
    public DateTime UpdateTime { get; set; }

    public bool IsDelete { get; set; } = false;
}