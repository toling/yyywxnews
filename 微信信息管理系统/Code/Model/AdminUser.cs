﻿using FreeSql.DataAnnotations;

namespace 微信信息管理系统.Code.Model;

[Index("uk_UserName", nameof(UserName), true)]
public class AdminUser : EntityBase
{
    public string UserName { get; set; }
    public string Password { get; set; }
    public string Remark { get; set; }
}
