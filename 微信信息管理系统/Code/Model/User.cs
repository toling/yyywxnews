﻿using FreeSql.DataAnnotations;

namespace 微信信息管理系统.Code.Model;

[Index("uk_Open", nameof(OpenId), true)]
public class User : EntityBase
{
    public string Name { get; set; }
    public string CompanyName { get; set; }
    public string Tel { get; set; }

    public string NickName { get; set; }
    public string HeadImgUrl { get; set; }
    public string OpenId { get; set; }

    public UserState UserState { get; set; } = UserState.待审核;

    public string Remark { get; set; }
}

public enum UserState
{
    正常,
    待审核,
    锁定
}