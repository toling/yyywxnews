﻿using FreeSql.DataAnnotations;

namespace 微信信息管理系统.Code.Model;

[Index("uk_ClassName", nameof(Title), true)]
public class News : EntityBase
{
    public string Title { get; set; }
    [Column(DbType = "longtext")]
    public string Content { get; set; }
    public int ClassId { get; set; }
    public NewClass Class { get; set; }
    public NewState NewState { get; set; } = NewState.显示;
    
    public string Files { get; set; }

    public string Remark { get; set; }
}

public enum NewState
{
    隐藏,
    显示
}