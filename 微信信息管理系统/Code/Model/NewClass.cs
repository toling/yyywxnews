﻿using FreeSql.DataAnnotations;

namespace 微信信息管理系统.Code.Model;

[Index("uk_ClassName", nameof(ClassName), true)]
public class NewClass : EntityBase
{
    public string ClassName { get; set; }

    public string Remark { get; set; }
}