﻿using FreeSql.DataAnnotations;

namespace 微信信息管理系统.Code.Model;

public class UserResDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string CompanyName { get; set; }
    public string Tel { get; set; }

    public string NickName { get; set; }
    public string HeadImg { get; set; }
    public string OpenId { get; set; }

    public UserState UserState { get; set; } = UserState.待审核;

    public string Remark { get; set; }
}