﻿using Furion.DynamicApiController;
using Microsoft.AspNetCore.Authorization;
using 微信信息管理系统.Code.Model;

namespace 微信信息管理系统.Code.Service;

[AppAuthorize]
public class NewsService : IDynamicApiController
{
    private readonly IFreeSql _fsql;

    public NewsService(IFreeSql fsql)
    {
        _fsql = fsql;
    }

    // 获取分页列表
    public async Task<Res<PageResult<News>>> PostPage(PageInput input)
    {
        var query = _fsql.Select<News>();
        if (!string.IsNullOrEmpty(input.Keywords))
        {
            query.Where(x =>
                x.Title.Contains(input.Keywords) ||
                x.Remark.Contains(input.Keywords)
            );
        }

        var result = await query.OrderByDescending(x => x.Id)
            .Include(x => x.Class)
            .Count(out long count)
            .Page(input.Page, input.PerPage)
            .ToListAsync();

        return new Res<PageResult<News>>(new PageResult<News>
            {
                count = (int) count,
                rows = result
            }
        );
    }

    [AllowAnonymous]
    public async Task<Res<object>> GetNewsList(int Id)
    {
        var News = await _fsql.Select<News>().Where(x => x.ClassId == Id).ToListAsync();
        var NewClass = await _fsql.Select<NewClass>().Where(x => x.Id == Id).ToOneAsync();

        return new Res<object>(new {rows = News, count = News.Count, NewClass = NewClass});
    }

    [AllowAnonymous]
    public async Task<Res<News>> Get(int id)
    {
        var News = await _fsql.Select<News>().Where(x => x.Id == id).Include(it => it.Class).ToOneAsync();
        if (News == null)
        {
            throw new Exception("您查看的资料不存在!");
        }

        return new Res<News>(News);
    }

    [AllowAnonymous]
    public async Task<Res<News>> Get(string OpenId, int id)
    {
        //判断是否有权限 根据OpenId
        var user = await _fsql.Select<User>().Where(it => it.OpenId == OpenId).ToOneAsync();
        if (user == null)
        {
            throw new Exception("您还未注册");
        }

        if (user.UserState != UserState.正常)
        {
            throw new Exception("您的账号没有通过审核!没有权限查看资料!");
        }

        var News = await _fsql.Select<News>().Where(x => x.Id == id && x.NewState == NewState.显示).Include(it => it.Class).ToOneAsync();
        if (News == null)
        {
            throw new Exception("您查看的资料不存在!");
        }

        return new Res<News>(News);
    }

    public async Task<Res<News>> Post(News News)
    {
        try
        {
            var result = await _fsql.InsertOrUpdate<News>().SetSource(News).ExecuteAffrowsAsync();
            return new Res<News>(News);
        }
        catch (Exception e)
        {
            return new Res<News>("信息已经提交过了！", false);
        }
    }

    public async Task<Res<News>> Put(News input)
    {
        var fq = _fsql.Update<News>();
        fq.Set(it => it.Remark == input.Remark);

        var result = await fq.Where(it => it.Id == input.Id).ExecuteAffrowsAsync();
        return new Res<News>();
    }

    public async Task<Res<News>> Delete(int id)
    {
        var result = await _fsql.Delete<News>(id).ExecuteAffrowsAsync();
        return new Res<News>(null);
    }
}