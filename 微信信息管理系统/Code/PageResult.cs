﻿namespace 微信信息管理系统.Code;

public class PageResult<T>
{
    public int count { get; set; }
    public List<T> rows { get; set; }
}