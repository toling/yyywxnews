﻿namespace 微信信息管理系统.Code;

public class PageInput
{
    public string OrderBy { get; set; } = "Id";
    public OrderDirType OrderDir { get; set; }
    public int Page { get; set; } = 1;
    public int PerPage { get; set; } = 20;
    public string Keywords { get; set; }
}

public enum OrderDirType
{
    Desc,
    Asc
}