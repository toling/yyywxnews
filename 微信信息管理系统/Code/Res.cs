﻿namespace 微信信息管理系统.Code;

public class Res<T>
{
    // 返回success
    public Res(bool success = true)
    {
        this.status = success ? 0 : -1;
        this.msg = "成功!";
    }

    public Res(int status, string msg, T data)
    {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    // 返回success
    public Res(string msg, bool success = true)
    {
        this.status = success ? 0 : -1;
        this.msg = msg;
    }

    // 返回success
    public Res(T data)
    {
        this.status = 0;
        this.msg = "成功!";
        this.data = data;
    }

    public int status { get; set; } = 0;
    public object msg { get; set; }
    public T data { get; set; }
}