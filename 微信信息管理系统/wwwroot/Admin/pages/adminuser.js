(function () {
    const response =
        {
            "type": "page",
            "name": "user",
            "debug": true,
            "body": [
                {
                    "label": "添加",
                    "type": "button",
                    "actionType": "dialog",
                    "level": "primary",
                    "className": "m-b-sm",
                    "dialog": {
                        "title": "添加用户表单",
                        "body": {
                            "type": "form",
                            "api": "post:/api/adminuser",
                            "body": [
                                {
                                    "type": "input-text",
                                    "name": "UserName",
                                    "label": "用户名",
                                    "required": true
                                },
                                {
                                    "type": "input-password",
                                    "name": "Password",
                                    "label": "密码",
                                    "required": true
                                },
                                {
                                    "type": "input-text",
                                    "name": "Remark",
                                    "label": "备注"
                                }
                            ]
                        }
                    }
                },
                {
                    "type": "crud",
                    "name": "user",
                    "initFetch": true,
                    "api": {
                        "method": "post",
                        "url": "/api/adminuser/page"
                    },
                    "alwaysShowPagination": true,
                    "perPage": 15,
                    "syncLocation": false,
                    "filter": {
                        "title": "条件搜索",
                        "body": [
                            {
                                "type": "input-text",
                                "name": "keywords",
                                "placeholder": "通过关键字模糊搜索"
                            }
                        ]
                    },
                    "columns": [
                        {
                            "name": "Id",
                            "label": "ID",
                            "width": 80
                        },
                        {
                            "name": "UserName",
                            "label": "用户名",
                            "sortable": true
                        },
                        {
                            "name": "Remark",
                            "label": "备注",
                            "required": true
                        },
                        {
                            "name": "CreateTime",
                            "label": "添加时间",
                            "width": 150
                        },
                        {
                            "name": "UpdateTime",
                            "label": "最后更新时间",
                            "width": 150
                        },
                        {
                            "type": "operation",
                            "label": "操作",
                            "width": 180,
                            "buttons": [
                                {
                                    "label": "修改",
                                    "type": "button",
                                    "actionType": "dialog",
                                    "dialog": {
                                        "title": "修改密码表单",
                                        "body": {
                                            "type": "form",
                                            "api": {
                                                "method": "put",
                                                "url": "/api/adminuser",
                                                data:{
                                                    "&": "$$",
                                                    "Password": "$PasswordNew"
                                                }
                                            },
                                            "body": [
                                                {
                                                    "type": "hidden",
                                                    "name": "Id",
                                                    "value": "${Id}"
                                                },
                                                {
                                                    "type": "input-password",
                                                    "name": "PasswordNew",
                                                    "label": "密码",
                                                    "suffix": "填写则修改密码"
                                                },
                                                {
                                                    "type": "input-text",
                                                    "name": "Remark",
                                                    "label": "备注",
                                                    "required": true
                                                }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "visibleOn": "UserName!='admin'",
                                    "label": "删除",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "danger",
                                    "confirmText": "确认要删除？",
                                    "api": "delete:/api/adminuser/${Id}"
                                }
                            ]
                        }
                    ]
                }
            ]
        }


    window.jsonpCallback && window.jsonpCallback(response);
})();
