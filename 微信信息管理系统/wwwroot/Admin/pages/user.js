(function () {
    const response =
        {
            "type": "page",
            "name": "user",
            "debug": true,
            "body": [
                
                {
                    "type": "crud",
                    "name": "user",
                    "initFetch": true,
                    "api": {
                        "method": "post",
                        "url": "/api/user/page"
                    },
                    "alwaysShowPagination": true,
                    "perPage": 15,
                    "syncLocation": false,
                    "filter": {
                        "title": "条件搜索",
                        "body": [
                            {
                                "type": "input-text",
                                "name": "keywords",
                                "placeholder": "通过关键字模糊搜索"
                            }
                        ]
                    },
                    "columns": [
                        {
                            "label": "ID",
                            "type": "text",
                            "name": "Id"
                        },
                        {
                            "label": "昵称",
                            "type": "text",
                            "name": "NickName"
                        },
                        {
                            "label": "头像",
                            "type": "avatar",
                            "name": "HeadImgUrl",
                            "src": "$HeadImgUrl"
                        },
                        {
                            "label": "微信ID",
                            "type": "text",
                            "name": "OpenId"
                        },
                        {
                            "label": "状态",
                            "type": "mapping",
                            "name": "UserState",
                            "map": {
                                "0": "<span class='label label-success'>正常</span>",
                                "1": "<span class='label label-info'>待审核</span>",
                                "2": "<span class='label label-warning'>被锁定</span>"
                            }
                        },
                        {
                            "label": "备注",
                            "type": "text",
                            "name": "Remark"
                        },
                        {
                            "label": "创建时间",
                            "type": "text",
                            "name": "CreateTime"
                        },
                        {
                            "label": "最后修改",
                            "type": "text",
                            "name": "UpdateTime"
                        },
                        {
                            "type": "operation",
                            "label": "操作",
                            "width": 180,
                            "buttons": [
                                {
                                    "visibleOn": "UserState==1",
                                    "label": "审核用户",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "success",
                                    "confirmText": "确认要审核？",
                                    "api": "post:/api/user/SetState/${Id}/0"
                                },
                                {
                                    "visibleOn": "UserState==0",
                                    "label": "锁定用户",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "warning",
                                    "confirmText": "确认要锁定用户？",
                                    "api": "post:/api/user/SetState/${Id}/2"
                                },
                                {
                                    "visibleOn": "UserState==2",
                                    "label": "恢复正常",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "success",
                                    "confirmText": "确认要删除？",
                                    "api": "post:/api/user/SetState/${Id}/0"
                                },
                                {
                                    "label": "删除",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "danger",
                                    "confirmText": "确认要删除？",
                                    "api": "delete:/api/user/${Id}"
                                }
                            ]
                        }
                    ]
                }
            ]
        }


    window.jsonpCallback && window.jsonpCallback(response);
})();
