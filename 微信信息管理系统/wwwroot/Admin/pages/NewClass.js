(function () {
    const response =
        {
            "type": "page",
            "name": "user",
            "debug": true,
            "body": [
                {
                    "label": "添加分类",
                    "type": "button",
                    "actionType": "dialog",
                    "level": "primary",
                    "className": "m-b-sm",
                    "dialog": {
                        "title": "添加",
                        "body": {
                            "type": "form",
                            "api": "post:/api/newClass",
                            "body": [
                                {
                                    "type": "input-text",
                                    "name": "ClassName",
                                    "label": "分类名",
                                    "required": true
                                },
                                {
                                    "type": "input-text",
                                    "name": "Remark",
                                    "label": "备注"
                                }
                            ]
                        }
                    }
                },
                {
                    "type": "crud",
                    "name": "user",
                    "initFetch": true,
                    "api": {
                        "method": "post",
                        "url": "/api/NewClass/page"
                    },
                    "alwaysShowPagination": true,
                    "perPage": 15,
                    "syncLocation": false,
                    "filter": {
                        "title": "条件搜索",
                        "body": [
                            {
                                "type": "input-text",
                                "name": "keywords",
                                "placeholder": "通过关键字模糊搜索"
                            }
                        ]
                    },
                    "columns": [
                        {
                            "label": "ID",
                            "type": "text",
                            "name": "Id"
                        },
                        {
                            "label": "分类名称",
                            "type": "text",
                            "name": "ClassName"
                        },
                        {
                            "label": "备注",
                            "type": "text",
                            "name": "Remark"
                        },
                        {
                            "label": "创建时间",
                            "type": "text",
                            "name": "CreateTime"
                        },
                        {
                            "label": "最后修改",
                            "type": "text",
                            "name": "UpdateTime"
                        },
                        {
                            "type": "operation",
                            "label": "操作",
                            "width": 180,
                            "buttons": [
                                {
                                    "label": "修改",
                                    "type": "button",
                                    "actionType": "dialog",
                                    "dialog": {
                                        "title": "修改",
                                        "body": {
                                            "type": "form",
                                            "api": {
                                                "method": "put",
                                                "url": "/api/newClass",
                                            },
                                            "body": [
                                                {
                                                    "type": "hidden",
                                                    "name": "Id",
                                                    "value": "${Id}"
                                                },
                                                {
                                                    "type": "input-text",
                                                    "name": "ClassName",
                                                    "label": "分类名称",
                                                    "required": true,
                                                },
                                                {
                                                    "type": "input-text",
                                                    "name": "Remark",
                                                    "label": "备注",
                                                    "required": true
                                                }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "label": "删除",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "danger",
                                    "confirmText": "确认要删除？",
                                    "api": "delete:/api/newClass/${Id}"
                                },
                                {
                                    "label": "链接/二维码",
                                    "type": "button",
                                    "actionType": "dialog",
                                    "level": "default",
                                    "dialog": {
                                        "title": "链接/二维码",
                                        "body": [
                                            {
                                                "type": "qr-code",
                                                "codeSize": 300,
                                                "value": "${window:location.origin}/nlist.html?Id=${Id}"
                                            },
                                            {
                                                type: "tpl",
                                                tpl: "${window:location.origin}/nlist.html?Id=${Id}"
                                            }, 
                                            {
                                                "label": "复制链接",
                                                "type": "button",
                                                "actionType": "copy",
                                                "content": "${window:location.origin}/nlist.html?Id=${Id}"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }


    window.jsonpCallback && window.jsonpCallback(response);
})();
