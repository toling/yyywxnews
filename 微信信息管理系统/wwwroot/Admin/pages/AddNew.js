﻿(function () {
    const response =
        {
            "type": "page",
            "body": [
                {
                    "type": "form",
                    "initFetchOn": "${window:location.href|isMatch:'Id=':true:false}",
                    "initApi": "/api/news/${Id}",
                    // "initApi": "${window:location.href|isMatch:'Id=':'/api/news/'${Id}:''}",
                    "name": "editform",
                    "affixFooter": true,
                    "api": {
                        method: "POST",
                        url: "/api/news"
                    },
                    "title": "水平模式",
                    "mode": "horizontal",
                    "redirect": "news",
                    "body": [
                        {
                            "type": "select",
                            "name": "ClassId",
                            "label": "选择分类",
                            "source": "/api/newclass/selectlist",
                            "required": true,
                            "size": "md"
                        },
                        {
                            "type": "input-text",
                            "name": "Title",
                            "label": "标题",
                            "required": true
                        },
                        {
                            "type": "input-rich-text",
                            "name": "Content",
                            "label": "内容",
                            receiver: "/api/tool/uploadfile/1"
                        },
                        {
                            "type": "input-file",
                            "name": "Files",
                            "multiple": true,
                            "drag": false,
                            "useChunk": false,
                            "maxSize": 1024 * 1024 * 2,
                            "label": "文件上传",
                            "accept": ".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.jpg,.jpeg,.png,.gif,.bmp,.zip,.rar",
                            "receiver": {
                                "url": "/api/tool/uploadfile/0"
                            }
                        },
                        {
                            "name": "NewState",
                            "type": "button-group-select",
                            "label": "状态",
                            "value": 1,
                            "required": true,
                            "options": [
                                {
                                    "label": "显示",
                                    "value": 1
                                },
                                {
                                    "label": "隐藏",
                                    "value": 0
                                }
                            ]
                        },
                        {
                            "name": "Remark",
                            "type": "textarea",
                            "label": "备注"
                        },
                        // {
                        //     type: "static",
                        //     style: {
                        //         display: "none !important"
                        //     },
                        //     "tpl": "<% if(location.href.indexOf('?Id=')==-1){for(var key in data){delete data[key];}}%>",
                        // },
                    ]
                }
            ]
        }

    window.jsonpCallback && window.jsonpCallback(response);
})();
