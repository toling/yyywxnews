(function () {
    const response =
        {
            "type": "page",
            "name": "user",
            "body": [

                {
                    "type": "crud",
                    "name": "user",
                    "initFetch": true,
                    "api": {
                        "method": "post",
                        "url": "/api/News/page"
                    },
                    "alwaysShowPagination": true,
                    "perPage": 15,
                    "syncLocation": false,
                    "filter": {
                        "title": "条件搜索",
                        "body": [
                            {
                                "type": "input-text",
                                "name": "keywords",
                                "placeholder": "通过关键字模糊搜索"
                            }
                        ]
                    },
                    "columns": [
                        {
                            "label": "ID",
                            "type": "text",
                            "name": "Id"
                        },
                        {
                            "label": "所属分类",
                            "type": "text",
                            "name": "Class.ClassName"
                        },
                        {
                            "label": "标题",
                            "type": "text",
                            "name": "Title"
                        },
                        {
                            "label": "状态",
                            "type": "mapping",
                            "name": "NewState",
                            "map": {
                                "0": "<span class='label label-info'>隐藏</span>",
                                "1": "<span class='label label-success'>显示</span>",
                            }
                        },
                        {
                            "label": "文件列表",
                            "type": "each",
                            "source": "${Files|split}",
                            "items": {
                                "type": "tpl",
                                "tpl": "<div><a target='_blank' href='${item}' class='mb-1 '>${index+1}：${item|split:/|last}</a></div>"
                            }
                        },
                        {
                            "label": "备注",
                            "type": "text",
                            "name": "Remark"
                        },
                        {
                            "label": "创建时间",
                            "type": "text",
                            "name": "CreateTime",
                            width: "150px"
                        },
                        {
                            "label": "最后修改",
                            "type": "text",
                            "name": "UpdateTime",
                            width: "150px"
                        },
                        {
                            "type": "operation",
                            "label": "操作",
                            "width": 180,
                            "buttons": [
                                {
                                    "label": "修改",
                                    "type": "button",
                                    "actionType": "link",
                                    "level": "success",
                                    "link": "editnew?Id=${Id}"
                                },
                                {
                                    "label": "删除",
                                    "type": "button",
                                    "actionType": "ajax",
                                    "level": "danger",
                                    "confirmText": "确认要删除？",
                                    "api": "delete:/api/News/${Id}"
                                },
                                {
                                    "label": "链接/二维码",
                                    "type": "button",
                                    "actionType": "dialog",
                                    "level": "default",
                                    "dialog": {
                                        "title": "链接/二维码",
                                        "body": [
                                            {
                                                "type": "qr-code",
                                                "codeSize": 300,
                                                "value": "${window:location.origin}/Detail.html?Id=${Id}"
                                            },
                                            {
                                                type: "tpl",
                                                tpl: "${window:location.origin}/Detail.html?Id=${Id}"
                                            },{
                                                "label": "复制链接",
                                                "type": "button",
                                                "actionType": "copy",
                                                "content": "${window:location.origin}/Detail.html?Id=${Id}"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }


    window.jsonpCallback && window.jsonpCallback(response);
})();
