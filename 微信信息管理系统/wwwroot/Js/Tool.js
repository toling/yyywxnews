﻿//url 编码
function encode(str) {
    return encodeURIComponent(str).replace(/'/g, '%27').replace(/"/g, '%22');
}

//url 解码
function decode(str) {
    return decodeURIComponent(str.replace(/\+/g, ' '));
}

function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]);
    return null; //返回参数值
}